package com.dh.ssiservice.Services;

import com.dh.ssiservice.model.Category;

import java.util.List;
import java.util.Optional;

public interface GenericService<T> {
    List<T> findAll();

    T findById(Long id);
}
