package com.dh.ssiservice.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.ssiservice.model.Position;

public interface PositionRepository extends CrudRepository<Position, Long> {
}
