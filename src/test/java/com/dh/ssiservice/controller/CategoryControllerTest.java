package com.dh.ssiservice.controller;

import com.dh.ssiservice.Services.CategoryService;
import com.dh.ssiservice.model.Category;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class CategoryControllerTest {
    @Mock
    CategoryService categoryService;
    @Mock
    private Model model;

    @InjectMocks
    CategoryController categoryController;
    private ArrayList<Category> categoryList;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        categoryList = new ArrayList<>();
        categoryList.add(new Category());
        when(categoryService.findAll()).thenReturn(categoryList);

    }

    @Test
    public void testGetCategories() throws Exception {
        ArgumentCaptor<List<Category>> argumentCaptor= ArgumentCaptor.forClass((Class<ArrayList<Category>>)categoryList.getClass());
        String result = categoryController.getCategories(null, model);
        String expectedView = "categories";
        assertEquals(expectedView, result);
        assertEquals(expectedView, expectedView);
        verify(categoryService,times(1)).findAll();
        verify(model,times(1)).addAttribute(eq(expectedView), eq(categoryList));

        verify(model,times(1)).addAttribute(eq("categories"), argumentCaptor.capture());
        List<Category> capturedCategories = argumentCaptor.getValue();
        assertEquals(capturedCategories.size(),1);
    }


    @Test
    public void testGetCategoriesById() throws Exception {
        when(categoryService.findById(anyLong())).thenReturn(null);

        String result = categoryController.getCategoriesById(Long.valueOf(1), model);
        assertEquals("category", result);
    }
}
