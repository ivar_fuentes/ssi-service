package com.dh.ssiservice.repositories;

import com.dh.ssiservice.model.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee,Long> {
}
