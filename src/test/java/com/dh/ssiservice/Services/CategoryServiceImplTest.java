package com.dh.ssiservice.Services;

import com.dh.ssiservice.model.Category;
import com.dh.ssiservice.repositories.CategoryRepository;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.*;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

public class CategoryServiceImplTest {

    private static final String OTRA_CAT = "OTRACAT";
    private List<Category> categorySet;

    @Mock
    CategoryRepository categoryRepository;
    @InjectMocks
    CategoryServiceImpl categoryServiceImpl;

    private Category category= new Category();

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        categorySet=new ArrayList<>();
        Category category = new Category();
        category.setName(OTRA_CAT);
        categorySet.add(category);

        when(categoryRepository.findAll()).thenReturn(categorySet);
    }

    @Test
    public void testGetCategories() {
        List<Category> result = categoryServiceImpl.findAll();
        //verify(categoryRepository,times(2)).findAll();
        verify(categoryRepository).findAll();
        assertEquals(result, categorySet);
        assertEquals(result.get(0).getName(),OTRA_CAT);
    }

    @Test
    public void testFindByCode() {
        when(categoryRepository.findByCode(any())).thenReturn(Optional.of(categorySet));

        List<Category> result = categoryServiceImpl.findByCode("code");
        assertEquals(result, Collections.singletonList(categorySet));
    }

    @Test
    public void testFindById() {
        when(categoryRepository.findAllById(anyLong())).thenReturn(Optional.of(category));
        Category result = categoryServiceImpl.findById(Long.valueOf(1));
        assertEquals(result, category);
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme