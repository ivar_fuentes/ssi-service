package com.dh.ssiservice.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.ssiservice.model.Contract;

public interface ContractRepository extends CrudRepository<Contract, Long> {
}
