package com.dh.ssiservice.Services;

import com.dh.ssiservice.model.Employee;

public interface EmployeeService extends GenericService<Employee> {

}
