package com.dh.ssiservice.Services;

import com.dh.ssiservice.model.Category;
import com.dh.ssiservice.repositories.CategoryRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CategoryServiceImpl extends GenericServiceImpl<Category>  {
    private CategoryRepository categoryRepository;


    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public List<Category> findByCode(String code) {
        List<Category> categories = new ArrayList<>();
        categoryRepository.findByCode(code).get().iterator().forEachRemaining(categories::add);
        return categories;
    }

    @Override
    protected CrudRepository<Category, Long> getRepository() {
        return categoryRepository;
    }
}
