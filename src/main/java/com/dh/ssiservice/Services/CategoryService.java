package com.dh.ssiservice.Services;

import com.dh.ssiservice.model.Category;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface CategoryService extends GenericService<Category>{

    List<Category> findByCode(String code);

}
