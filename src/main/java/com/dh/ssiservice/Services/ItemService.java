/**
 * @author: Ivar.
 */

package com.dh.ssiservice.Services;

import com.dh.ssiservice.model.Item;

public interface ItemService extends GenericService<Item> {
}
